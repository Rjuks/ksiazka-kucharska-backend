const nodemailer = require('nodemailer')
require('dotenv').config()

const emailOptionsOnCreateAccount = (userData: any, decryptedPassword: string) => {
    return {
        from: process.env.NODEMAILER_USER,
        to: [userData.email],
        subject: 'Utworzyłeś konto na aplikacji - Książka kucharska',
        html: `<h2>Cześć ${userData.name}</h2> 
        <p>Udało Ci się dołączyć do aplikacji Książka kucharska. Twoje dane do logowania: Login: <b>${userData.username}</b> i Hasło: <b>${decryptedPassword}</b></p>`,
    }
}

const emailOptionsOnForgotPassword = (userData: any, token: string) => {
    return {
        from: process.env.NODEMAILER_USER,
        to: [userData.email],
        subject: 'Reset hasła - Książka kucharska',
        html: `<h2>Cześć ${userData.name}</h2> 
        <p>Jeśli chcesz zresetować hasło to kliknij w odnośnik - <b>${`${process.env.FRONT_APP_URL_LOCAL}/reset-password?token=${token}`}</b></p>`,
    }
}

const emailOptionsOnSuccessfulRecipeAdd = (userData: any, recipeData: any) => {
    return {
        from: process.env.NODEMAILER_USER,
        to: [userData.email],
        subject: 'Twój przepis został zaakceptowany - Książka kucharska',
        html: `<h2>Cześć ${userData.name}</h2> 
        <p>Twój przepis "${recipeData.title}" został zaakceptowany przez administracje. Kliknij aby przejść do przepisu - <b>${`${process.env.FRONT_APP_URL_LOCAL}/recipe/${recipeData._id}`}</b></p>`,
    }
}

const emailOptionsOnDeclineRecipeAdd = (userData: any, recipeData: any) => {
    return {
        from: process.env.NODEMAILER_USER,
        to: [userData.email],
        subject: 'Twój przepis został usunięty - Książka kucharska',
        html: `<h2>Cześć ${userData.name}</h2> 
        <p>Twój przepis "${recipeData.title}" został usunięty bądz odrzucony przez administracje.</p>`,
    }
}

const emailOptionsOnTooManyLoginAttempts = (userData: any) => {
    return {
        from: process.env.NODEMAILER_USER,
        to: [userData.email],
        subject: 'Przekroczono liczbę błędnych logowań - Książka kucharska',
        html: `<h2>Cześć ${userData.name}</h2> 
        <p>Twoje konto zostało <strong>zablokowane na 30 minut</strong>, ponieważ została przekroczona ilość błędnych prób logowań.
         Jeżeli to nie byłeś Ty, zresetuj jak najszybciej hasło na stronie logowania poprzez kliknięcie <strong>Zresetuj hasło</strong> - <b>http://localhost:3000/login</b> </p>`,
    }
}


const sendEmail = async (mailOptions: any) => {
    try {
        //transporter bierze object api konfiguracje,
        const transporter = nodemailer.createTransport({
            host: 'smtp.gmail.com',
            service: 'gmail',
            port: 587,
            secure: false,
            auth: {
                user: process.env.NODEMAILER_USER,
                pass: process.env.NODEMAILER_PASS,
            },
            tls: {
                rejectUnauthorized: false,
            },
        })

        await transporter.sendMail(mailOptions, function (err: any, data: any) {
            if (err) {
                return console.log('Nodemailer errors', err)
            }

            return console.log('Email sent')
        })
    } catch (error) {
        console.log('Nodemailer error:', error)
    }
}

module.exports = {
    emailOptionsOnCreateAccount,
    emailOptionsOnForgotPassword,
    emailOptionsOnSuccessfulRecipeAdd,
    emailOptionsOnDeclineRecipeAdd,
    emailOptionsOnTooManyLoginAttempts,
    sendEmail,
}
