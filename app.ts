const express = require('express')
const app = express()
const cors = require('cors')
const mongoose = require('mongoose')
const dotenv = require('dotenv')
const cookieParser = require('cookie-parser')

const recipeRoute = require('./routes/recipe')
const authRoute = require('./routes/auth')
const userRoute = require('./routes/user')

// Enable CORS
app.use(
    cors({
        origin: 'http://localhost:3000',
        credentials: true,
    })
)

app.use(express.json({ limit: '50mb' }))
app.use(cookieParser())

dotenv.config()

// Routes
app.use('/auth', authRoute)
app.use('/recipe', recipeRoute)
app.use('/user', userRoute)

app.listen(process.env.PORT || 5000, () => {
    console.log('Backend server is running!')
})

mongoose
    .connect(process.env.MONGO_URL)
    .then(() => console.log('DB connection successful!'))
    .catch((error: Error) => {
        console.log(error)
    })
