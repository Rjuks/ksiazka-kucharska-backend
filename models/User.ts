import mongoose = require('mongoose')

const ResetPasswordSchema = new mongoose.Schema(
    {
        token: String,
        expirationTime: Date,
    },
    { _id: false }
)

const LoginAttempts = new mongoose.Schema(
    {
        attempts: Number,
        isBlocked: Boolean,
    },
    { timestamps: true, _id: false }
)


const UserSchema = new mongoose.Schema({
    username: { type: String, required: true, unique: true },
    name: { type: String, required: true },
    password: { type: String, required: true },
    email: { type: String, required: true, unique: true },
    isAdmin: {
        type: Boolean,
        default: false,
    },
    resetToken: { type: ResetPasswordSchema, required: false },
    loginAttempts: { type: LoginAttempts, required: false, },
    favouriteRecipes: { type: [String], required: true },
})

module.exports = mongoose.model('User', UserSchema)
