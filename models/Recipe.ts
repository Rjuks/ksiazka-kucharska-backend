import mongoose = require('mongoose')

const AuthorSchema = new mongoose.Schema(
    {
        id: { type: String, required: true },
        name: { type: String, required: true },
    },
    { _id: false }
)

const RecipeSchema = new mongoose.Schema(
    {
        author: { type: AuthorSchema, required: true },
        title: { type: String, required: true },
        ingredients: [{ type: String, required: true }],
        image: { type: String, required: true },
        difficulty: {
            type: String,
            enum: ['easy', 'medium', 'hard'],
            required: true,
        },
        preparationTime: { type: String, required: true },
        calorificValue: { type: Number, required: true },
        preparingMethod: { type: [String], required: true },
        category: {
            type: String,
            enum: ['breakfast', 'dinner', 'supper', 'snack', 'dessert', 'oriental', 'soup'],
            required: true,
        },
        isAccepted: { type: Boolean, default: false },
    },
    { timestamps: true }
)

module.exports = mongoose.model('Recipe', RecipeSchema)
