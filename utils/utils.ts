export const millisToMinutesAndSeconds = (millis: number) => {
    const minutes = Math.floor(millis / 60000);
    const seconds = ((millis % 60000) / 1000).toFixed(0);
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    return minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
}

export const addSomeMinutesToTime = (startTime: Date, minutesToAdd: number) => {
    const dateObj = new Date(startTime);
    const newDateInNumber = dateObj.setMinutes(dateObj.getMinutes() + minutesToAdd);
    const processedTime = new Date(newDateInNumber).toISOString();
    return processedTime;
}
