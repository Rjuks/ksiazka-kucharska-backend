import express = require('express')
const router = express.Router()
const auth = require('../controllers/auth')
const verify = require('./verifyToken')

router.post('/register', auth.registerUser)

router.post('/login', auth.loginUser)

router.get('/logout', auth.logoutUser)

router.get('/checkToken', verify.verifyToken, auth.checkUserToken)

router.post('/forgot-password', auth.forgotPassword)

router.post('/reset-password', auth.resetPassword)

module.exports = router
