import express = require('express')
const router = express.Router()
const user = require('../controllers/user')
const verify = require('./verifyToken')


router.get('/get-data', verify.verifyToken, user.getData)


module.exports = router
