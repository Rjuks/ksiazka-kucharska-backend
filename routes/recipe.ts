import express = require('express')
const router = express.Router()
const recipe = require('../controllers/recipe')
const verify = require('./verifyToken')


router.get('/all', verify.verifyTokenAndAdmin, recipe.getAllRecipes)

router.get('/all-accepted', recipe.getAllAcceptedRecipes)

router.get('/my-recipes', verify.verifyToken, recipe.getUserRecipes)

router.get('/my-favourite-recipes', verify.verifyToken, recipe.getUserFavouriteRecipes)

router.get('/all/:category', recipe.getRecipesByCategory)

router.get('/:id', recipe.getRecipeByID)

router.post('/add',  verify.verifyToken, recipe.addRecipe)

router.delete('/:id',  verify.verifyToken, recipe.deleteRecipeByID)

router.patch('/',  verify.verifyToken, recipe.patchRecipe)

router.post('/add-to-favourites/:id',  verify.verifyToken, recipe.addToFavourites)

router.post('/remove-from-favourites/:id',  verify.verifyToken, recipe.removeFromFavourites)


module.exports = router
