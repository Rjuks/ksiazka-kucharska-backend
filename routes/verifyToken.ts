import { NextFunction, Request, Response } from 'express'

const jwt = require('jsonwebtoken')

//todo put proper interface for user
export interface AuthRequest extends Request {
    user: any
}

const verifyToken = (req: AuthRequest, res: Response, next: NextFunction) => {
    const cookieToken = req.cookies.accessToken

    if (cookieToken) {
        // weryfikujemy z pomocą secret key'a czy token jest valid
        jwt.verify(cookieToken, process.env.JWT_SECRET, (error: any, user: any) => {
            if (error) res.status(403).send('Token is not valid!')

            req.user = user

            next()
        })
    } else {
        return res.status(401).send('You are not authenticated!')
    }
}

// weryfikacja + autoryzacja czy użytkownik jest danym użytkownikiem (np jeśli tylko on może usunąć siebie) lub admin
const verifyTokenAndAuthorization = (req: AuthRequest, res: Response, next: NextFunction) => {
    verifyToken(req, res, () => {
        if (req.user.id === req.params.id || req.user.isAdmin) {
            next()
        } else {
            res.status(403).json('You are not allowed to do that!')
        }
    })
}

// weryfikacja + autoryzacja czy użytkownik jest adminem
const verifyTokenAndAdmin = (req: AuthRequest, res: Response, next: NextFunction) => {
    verifyToken(req, res, () => {
        if (req.user.isAdmin) {
            next()
        } else {
            res.status(403).json('You are not allowed to do that!')
        }
    })
}

module.exports = {
    verifyToken,
    verifyTokenAndAuthorization,
    verifyTokenAndAdmin,
}
