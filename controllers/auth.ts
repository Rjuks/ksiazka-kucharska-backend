import { Request, Response } from 'express'
import { AuthRequest } from '../routes/verifyToken'
import { millisToMinutesAndSeconds, addSomeMinutesToTime} from '../utils/utils'

const User = require('../models/User')
const CryptoJS = require('crypto-js')
const jwt = require('jsonwebtoken')
const mailer = require('../services/nodemailer')

exports.registerUser = async (req: Request, res: Response) => {
    const newUser = new User({
        username: req.body.username,
        email: req.body.email,
        name: req.body.name,
        password: CryptoJS.AES.encrypt(req.body.password, process.env.PASSWORD_SECRET).toString(),
    })

    const usernameExistsInDB = await User.findOne({
        username: req.body.username,
    })
    const userEmailExistsInDB = await User.findOne({ email: req.body.email })

    if (userEmailExistsInDB || usernameExistsInDB) {
        return res.status(409).send({
            message: 'Konto z taką nazwą użytkownika lub emailem już istnieje',
        })
    }

    const { name } = req.body

    try {
        const savedUser = await newUser.save()
        const sendEmailToUser = await mailer.sendEmail(mailer.emailOptionsOnCreateAccount(savedUser, req.body.password))
        res.status(201).send({ message: 'Rejestracja przebiegła pomyślnie', data: { name } })
    } catch (error: unknown) {
        res.status(500).send({
            message: 'Błąd podczas rejestracji. Skontaktuj się z administratorem',
        })
    }
}

exports.loginUser = async (req: Request, res: Response) => {
    const loginAttemptsInitialValuesObject = {
        attempts: 1,
        isBlocked: false
    }

    const accountBanDuration = 30
    const safeTimeForLoginAttempts = 15
    const maxLoginAttempts = 5


    try {
        const userInDB = await User.findOne({ username: req.body.username })

        if (!userInDB) {
            return  res.status(401).send({ message: 'Taki użytkownik nie istnieje.' })
        }


        const decryptOriginalPassword = CryptoJS.AES.decrypt(userInDB.password, process.env.PASSWORD_SECRET).toString(
            CryptoJS.enc.Utf8
        )

        if (decryptOriginalPassword !== req.body.password) {


            // Jeżeli użytkownik nie ma obiektu loginAttempts
            if(!userInDB.loginAttempts) {
                const updateUserLoginAttempts = await User.findOneAndUpdate({ _id: userInDB._id }, { loginAttempts: loginAttemptsInitialValuesObject })
                return res.status(401).send({ message: 'Niepoprawne dane logowania' })
            }


            // Aktualny czas (zamienie w milisekundy), poniewaz nie mozna porownac stringow
            const currentDate = new Date().getTime()
            // Bierze czas z ostatniej proby logowania i dodaje do niej 30 minut
            const addMinutesToLastLoginAttempt = new Date(addSomeMinutesToTime(userInDB.loginAttempts.updatedAt, accountBanDuration )).getTime()
            // Zmienna ktora przechowuje pozostaly czas bana
            const remainingBanTime = millisToMinutesAndSeconds(addMinutesToLastLoginAttempt - currentDate )
            // Dodaje do createdAt 15 minut, czas w którym trzeba poprawnie się zalogować (podczas pierwszej błędnej próby logowania)
            const addMinutesToCreatedAtLoginAttempt = new Date(addSomeMinutesToTime(userInDB.loginAttempts.createdAt, safeTimeForLoginAttempts)).getTime()


            //Sprawdza czy mineło 30 minut, jeśli tak to reset obiektu loginAttempt
            if(currentDate >= addMinutesToLastLoginAttempt) {
                const resetloginAttemptsObject = {
                    attempts: 0,
                    isBlocked: false
                }
                const updateUserLoginAttempts = await User.findOneAndUpdate({ _id: userInDB._id }, { loginAttempts: resetloginAttemptsObject })
                userInDB.loginAttempts = resetloginAttemptsObject
            }

            // Jeżeli konto jest zablokowane
            if(userInDB.loginAttempts.isBlocked) {
                return res.status(401).send({ message: `Twoje konto jest zablokowane, pozostały czas - ${remainingBanTime} minut` })
            }


            // Jeżeli próby logowania są równe 5 i czy nie mineło 15 minut od pierwszej próby logowania
            if(userInDB.loginAttempts.attempts >= maxLoginAttempts  && currentDate < addMinutesToCreatedAtLoginAttempt ) {
                const updateUserLoginAttempts = await User.findOneAndUpdate({ _id: userInDB._id }, { 'loginAttempts.isBlocked': true })
                const sendEmailToUser = await mailer.sendEmail(mailer.emailOptionsOnTooManyLoginAttempts(userInDB))
                return res.status(401).send({ message: 'Przekroczyłeś dozwoloną liczbę logowań w ciągu 15 minut. Twoje konto zostało zablokowane na 30 minut' })
            }

            // Jeżeli jest ponizej 5 prób logowań i czy konto nie jest zablookwane
            if(userInDB.loginAttempts.attempts < maxLoginAttempts && !userInDB.loginAttempts.isBlocked) {
                const updateUserLoginAttempts = await User.findOneAndUpdate({ _id: userInDB._id }, { 'loginAttempts.attempts': userInDB.loginAttempts?.attempts + 1, })
            }

            // Jeżeli mineło 15 minut i nie ma bana - resetuje wartości i czas utworzenia obiektu
            if(currentDate > addMinutesToCreatedAtLoginAttempt && !userInDB.loginAttempts.isBlocked) {
                const updateUserLoginAttempts = await User.findOneAndUpdate({ _id: userInDB._id }, { loginAttempts: loginAttemptsInitialValuesObject })
            }


          return res.status(401).send({ message: 'Niepoprawne dane logowania' })
        }


       if(userInDB.loginAttempts) {
            // Aktualny czas (zamienie w milisekundy), poniewaz nie mozna porownac stringow
            const currentDate = new Date().getTime()
            // Bierze czas z ostatniej proby logowania i dodaje do niej 30 minut
            const addMinutesToLastLoginAttempt = new Date(addSomeMinutesToTime(userInDB.loginAttempts.updatedAt, accountBanDuration )).getTime()
            // Zmienna ktora przechowuje pozostaly czas bana
            const remainingBanTime = millisToMinutesAndSeconds(addMinutesToLastLoginAttempt - currentDate )
            // Dodaje do createdAt 15 minut, czas w którym trzeba poprawnie się zalogować (podczas pierwszej błędnej próby logowania)
            const addMinutesToCreatedAtLoginAttempt = new Date(addSomeMinutesToTime(userInDB.loginAttempts.createdAt, safeTimeForLoginAttempts)).getTime()

            //Sprawdza czy mineło 30 minut, jeśli tak to reset obiektu loginAttempt
            if(currentDate >= addMinutesToLastLoginAttempt) {
                const resetloginAttemptsObject = {
                    attempts: 0,
                    isBlocked: false
                }
                const updateUserLoginAttempts = await User.findOneAndUpdate({ _id: userInDB._id }, { loginAttempts: resetloginAttemptsObject })
                userInDB.loginAttempts = resetloginAttemptsObject
            }

            // Jeżeli konto jest zablokowane
            if(userInDB.loginAttempts.isBlocked) {
                return res.status(401).send({ message: `Twoje konto jest zablokowane, pozostały czas - ${remainingBanTime} minut` })
            }

            // Jeżeli mineło 15 minut i nie ma bana - resetuje wartości i czas utworzenia obiektu
            if(currentDate > addMinutesToCreatedAtLoginAttempt && !userInDB.loginAttempts.isBlocked) {
                const updateUserLoginAttempts = await User.findOneAndUpdate({ _id: userInDB._id }, { loginAttempts: loginAttemptsInitialValuesObject })
            }
       }

        //Inicjacja JWT, payload = id, czy jest adminem i umię, podanie klucza sekretnego i wygaśnięcie 3 dni
        const accessToken = jwt.sign(
            {
                id: userInDB._id,
                isAdmin: userInDB.isAdmin,
                name: userInDB.name,
            },
            process.env.JWT_SECRET
        )

        // wsadzannie tokena do headeara, expires 5h, secure powinno byc na prodzie teraz nw czemu
        res.cookie('accessToken', accessToken, {
            maxAge: 18000000,
            httpOnly: true,
            // secure: true
        })

        const { name, isAdmin, _id } = userInDB

        res.status(200).send({
            message: 'Zalogowano pomyślnie',
            data: { name, isAdmin, id: _id },
        })
    } catch (error: unknown) {
        console.log(error,'jakiblont')
        res.status(500).send({
            message: 'Błąd podczas logowania. Skontaktuj się z administratorem',
        })
    }
}

exports.logoutUser = async (req: Request, res: Response) => {
    res.clearCookie('accessToken').send({ message: 'Wylogowano pomyślnie' })
}

exports.checkUserToken = async (req: AuthRequest, res: Response) => {
    const { name, isAdmin, id } = req.user

    res.status(200).send({ message: 'Check token', data: { name, isAdmin, id } })
}

exports.forgotPassword = async (req: AuthRequest, res: Response) => {
    const { email } = req.body

    const findUserByEmail = await User.findOne({ email: email })

    let userDataToUpdate
    let token

    if (!findUserByEmail) {
       return  res.status(409).send({ message: 'Błąd podczas resetowania hasła. Nie znaleziono adresu email' })
    }

    try {
        console.log('Tworzenie tokenu do resetu hasła...')
        token = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15)
        // Token ważny 1h
        userDataToUpdate = { token: token, expirationTime: Date.now() + 3600000 }
    } catch (error: any) {
        console.log(error, 'Błąd podczas szyfrowania tokenu')
        return res.status(500).send({
            message:
                'Błąd podczas resetowania hasła użytkownika. Spróbuj jeszcze raz lub skontaktuj się z administratorem',
        })
    }

    try {
        const userUpdated = await User.findByIdAndUpdate(findUserByEmail.id, {
            resetToken: userDataToUpdate,
        })
        console.log('Tokeny do resetu hasła zapisane pomyślnie...', 'success')
    } catch (err) {
        console.log(err, 'error')
        return res.status(500).send({
            message:
                'Błąd podczas resetowania hasła użytkownika. Spróbuj jeszcze raz lub skontaktuj się z administratorem',
        })
    }

    try {
        const sendEmailToUser = await mailer.sendEmail(mailer.emailOptionsOnForgotPassword(findUserByEmail, token))
        console.log('Link do resetu hasła pomyślnie wysłany. Sprawdz skrzynkę pocztową', 'success')
        return res.status(200).send({
            message: 'Link do resetu hasła pomyślnie wysłany. Sprawdz skrzynkę pocztową',
        })
    } catch (err) {
        return res.status(500).send({
            message: 'Błąd podczas resetowania hasła użytkownika. Spróbuj jeszcze raz lub skontaktuj się z administratorem',
        })
    }
}

exports.resetPassword = async (req: AuthRequest, res: Response) => {
    const { password } = req.body
    const token = req.query.token
    const userToUpdate = await User.findOne({ 'resetToken.token': token })


    if (!userToUpdate) {
        return res.status(409).send({
            message: 'Błąd podczas resetowania hasła. Link mógł stracić ważność',
        })
    }


    if (Date.now() >= userToUpdate.resetToken.expirationTime * 1000) {
        return res.status(409).send({
            message: 'Błąd podczas resetowania hasła. Upłynął czas ważności linku resetu hasła',
        })
    }

    try {
        const resetPassword = await User.findByIdAndUpdate(userToUpdate._id, {
            password: CryptoJS.AES.encrypt(password, process.env.PASSWORD_SECRET).toString(),
            resetToken: null,
        })
        res.status(200).send({ message: 'Hasło zresetowane pomyslnie' })
    } catch (error: unknown) {
        res.status(500).send({
            message: 'Błąd podczas resetowania hasła. Skontaktuj się z administratorem',
        })
    }
}
