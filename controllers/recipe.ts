import { Response, Request } from 'express'
import { AuthRequest } from '../routes/verifyToken'
const User = require('../models/User')
const mailer = require('../services/nodemailer')
const Recipe = require('../models/Recipe')
const cloudinary = require('../services/cloudinary')

exports.getAllRecipes = async (req: Request, res: Response) => {
    try {
        const allRecipes = await Recipe.find()
        res.status(200).send({ message: 'Przepisy pomyślnie pobrane', data: allRecipes })
    } catch (error: unknown) {
        res.status(500).send({
            message: 'Błąd podczas pobierania przepisów. Skontaktuj się z administratorem',
        })
    }
}

exports.getAllAcceptedRecipes = async (req: Request, res: Response) => {
    try {
        const allAcceptedRecipes = await Recipe.find({ isAccepted: true })
        res.status(200).send({
            message: 'Zaakceptowane przepisy pomyślnie pobrane',
            data: allAcceptedRecipes,
        })
    } catch (error: unknown) {
        res.status(500).send({
            message: 'Błąd podczas zaakceptowanych pobierania przepisów. Skontaktuj się z administratorem',
        })
    }
}

exports.getUserRecipes = async (req: AuthRequest, res: Response) => {
    try {
        const userRecipes = await Recipe.find({ 'author.id': req.user.id })
        res.status(200).send({
            message: 'Przepisy użytkownika pomyślnie pobrane',
            data: userRecipes,
        })
    } catch (error: unknown) {
        res.status(500).send({
            message: 'Błąd podczas pobierania przepisów użytkownika. Skontaktuj się z administratorem',
        })
    }
}

exports.getRecipeByID = async (req: Request, res: Response) => {
    try {
        const recipeByID = await Recipe.findById(req.params.id)
        res.status(200).send({ message: 'Przepis pobrany pomyślnie', data: recipeByID })
    } catch (error: unknown) {
        res.status(500).send({
            message: 'Błąd podczas pobierania przepisu. Skontaktuj się z administratorem',
        })
    }
}


exports.getRecipesByCategory = async (req: Request, res: Response) => {
    try {
        const recipesByCategory = await Recipe.find({
            category: req.params.category,
            isAccepted: true,
        })
        res.status(200).send({ message: 'Przepis pobrany pomyślnie', data: recipesByCategory })
    } catch (error: unknown) {
        res.status(500).send({
            message: 'Błąd podczas pobierania przepisu. Skontaktuj się z administratorem',
        })
    }
}

exports.addRecipe = async (req: Request, res: Response) => {
    const {
        author,
        title,
        ingredients,
        image,
        difficulty,
        preparationTime,
        calorificValue,
        preparingMethod,
        category,
        isAccepted,
    } = req.body



    try {
        const uploadCloudinaryImage = await cloudinary.uploader.upload(image, {
            upload_preset: 'images',
        })


        const newRecipe = new Recipe({
            author: author,
            title: title,
            ingredients: ingredients,
            image: uploadCloudinaryImage.public_id,
            difficulty: difficulty,
            preparationTime: preparationTime,
            calorificValue: calorificValue,
            preparingMethod: preparingMethod,
            category: category,
            isAccepted: isAccepted,
        })
        const savedRecipe = await newRecipe.save()
        res.status(200).send({ message: 'Przepis dodany pomyślnie', data: savedRecipe })
    } catch (error: unknown) {
        console.log(error)
        res.status(500).send({
            message: 'Błąd podczas tworzenia przepisu. Skontaktuj się z administratorem',
        })
    }
}
 
exports.deleteRecipeByID = async (req: Request, res: Response) => {

    const findRecipeByID = await Recipe.findOne({ _id: req.params.id })

    if (!findRecipeByID) {
        return  res.status(409).send({ message: 'Błąd podczas usuwania przepisu. Skontaktuj się z administratorem' })
    }

    const findUserByID = await User.findOne({ _id: findRecipeByID.author.id })

    if (!findUserByID) {
        return  res.status(409).send({ message: 'Błąd podczas usuwania przepisu. Skontaktuj się z administratorem' })
    }

    try {
        const deletedRecipeByID = await Recipe.findByIdAndDelete(req.params.id)


        const findAllUsersAndDeleteRecipeFromArray = User.find({}, function(err: any, res: any) {
            res.forEach((user: any) => {
                user.favouriteRecipes = user.favouriteRecipes.filter((item: string) => item !== req.params.id)
                user.save()
            });
        })


        const sendEmailToUser = await mailer.sendEmail(mailer.emailOptionsOnDeclineRecipeAdd(findUserByID, findRecipeByID))
        const deleteCloudinaryImage = await cloudinary.uploader.destroy(findRecipeByID.image, {
            upload_preset: 'images',
        })
        res.status(200).send({ message: 'Przepis został pomyślnie usunięty' })
    } catch (error: unknown) {
        console.log(error)
        res.status(500).send({
            message: 'Błąd podczas usuwania przepisu. Skontaktuj się z administratorem',
        })
    }
}

exports.patchRecipe = async (req: Request, res: Response) => {
    const updatedRecipe = req.body


    try {
        // Jeżeli przepis jest zaakceptowany, to update + wyślij maila
        if(req.body.isAccepted) {
            const updatedRecipeByID = await Recipe.findOneAndUpdate({ _id: req.body.id }, updatedRecipe, {
                upsert: true,
                new: true,
            })

            const findUserByID = await User.findOne({ _id: updatedRecipeByID.author.id })

            if (!findUserByID) {
                return  res.status(409).send({ message: 'Błąd podczas akceptowania przepisu. Skontaktuj się z administratorem' })
            }

            const sendEmailToUser = await mailer.sendEmail(mailer.emailOptionsOnSuccessfulRecipeAdd(findUserByID, updatedRecipeByID))
           return  res.status(200).send({
                message: 'Przepis został pomyślnie zaktualizowany',
                data: updatedRecipeByID,
            })
        }



        if(!req.body.isAccepted) {

            const findRecipeByID = await Recipe.findOne({ _id: req.body.id })

            if (!findRecipeByID) {
                return  res.status(409).send({ message: 'Błąd podczas akceptowania przepisu. Skontaktuj się z administratorem' })
            }


            const updatedRecipeByID = await Recipe.findOneAndUpdate({ _id: req.body.id }, updatedRecipe, {
                upsert: true,
                new: true,
            })

        // Jeśli użytkownik zaktualizował zdjęcie, usuń poprzednie z Cloudinary
        if(findRecipeByID.image !== updatedRecipe.image) {

            // Usuń poprzednie zdjęcie z Cloudinary
            const deleteCloudinaryImage = await cloudinary.uploader.destroy(findRecipeByID.image, {
                upload_preset: 'images',
            })

            // Upload nowego zdjęcia na cloudinary
            const uploadCloudinaryImage = await cloudinary.uploader.upload(updatedRecipe.image, {
                upload_preset: 'images',
            })

            const updatedRecipeByNewImage = {...updatedRecipe, image: uploadCloudinaryImage.public_id}

            const updatedRecipeByID = await Recipe.findOneAndUpdate({ _id: req.body.id }, updatedRecipeByNewImage, {
                upsert: true,
                new: true,
            })

            return  res.status(200).send({
                message: 'Przepis został pomyślnie zaktualizowany',
                data: updatedRecipeByID,
            })
        }


            return  res.status(200).send({
                message: 'Przepis został pomyślnie zaktualizowany',
                data: updatedRecipeByID,
            })
        }

    } catch (error: unknown) {
        res.status(500).send({
            message: 'Błąd podczas aktualizowania przepisu. Skontaktuj się z administratorem',
        })
    }
}


exports.addToFavourites = async (req: AuthRequest, res: Response) => {
    const findUserByID = await User.findOne({ _id: req.user.id })

    if (!findUserByID) {
        return  res.status(409).send({ message: 'Błąd podczas dodawania przepisu do ulubionych. Skontaktuj się z administratorem' })
    }

    if(findUserByID.favouriteRecipes.includes(req.params.id)) {
        const updatedUserFavouriteRecipeByID = await User.findOneAndUpdate({ _id: req.user.id }, { $pull: { favouriteRecipes: req.params.id }})
        return  res.status(200).send({ message: 'Ten przepis jest już polubiony' })
    }

    try {
        const updatedUserFavouriteRecipeByID = await User.findOneAndUpdate({ _id: req.user.id }, { $addToSet: { favouriteRecipes: req.params.id }})
        res.status(200).send({ message: 'Przepis został dodany do ulubionych' })
    } catch (error: unknown) {
        res.status(500).send({
            message: 'Błąd podczas dodawania przepisu do ulubionych. Skontaktuj się z administratorem',
        })
    }
}

exports.removeFromFavourites = async (req: AuthRequest, res: Response) => {
    const findUserByID = await User.findOne({ _id: req.user.id })

    if (!findUserByID) {
        return res.status(409).send({ message: 'Błąd podczas usuwania przepisu z ulubionych. Skontaktuj się z administratorem' })
    }

    try {
        //To jeśli juz jest to ID to usuwa
        if(findUserByID.favouriteRecipes.includes(req.params.id)) {
            const updatedUserFavouriteRecipeByID = await User.findOneAndUpdate({ _id: req.user.id }, { $pull: { favouriteRecipes: req.params.id }})
            return  res.status(200).send({ message: 'Przepis został usunięty z ulubionych' })
        }
    } catch (error: unknown) {
        res.status(500).send({
            message: 'Błąd podczas dodawania przepisu do ulubionych. Skontaktuj się z administratorem',
        })
    }
}



exports.getUserFavouriteRecipes = async (req: AuthRequest, res: Response) => {
    const findUserByID = await User.findOne({ _id: req.user.id })

    if (!findUserByID) {
        return  res.status(409).send({ message: 'Błąd podczas pobierania ulubionych przepisów. Skontaktuj się z administratorem' })
    }

    try {
        const findRecipesByIDs = await Recipe.find({ _id: findUserByID.favouriteRecipes, isAccepted: true})

        res.status(200).send({ message: 'Ulubione przepisy pomyślnie pobrane', data: findRecipesByIDs, })
    } catch (error: unknown) {
        res.status(500).send({
            message: 'Błąd podczas pobierania ulubionych przepisów. Skontaktuj się z administratorem',
        })
    }
}

