import { Response } from 'express'
const User = require('../models/User')
import { AuthRequest } from '../routes/verifyToken'

exports.getData = async (req: AuthRequest, res: Response) => {

    try {
        const findUserByID = await User.findOne({ _id: req.user.id })
        const { _id, username, name, email, favouriteRecipes, isAdmin } = findUserByID
        res.status(200).send({ message: 'Dane użytkownika pomyślnie pobrane', data: { id: _id, username, name, email, favouriteRecipes, isAdmin}, })
    } catch (error: unknown) {
        res.status(500).send({
            message: 'Błąd podczas pobierania danych użytkownika. Skontaktuj się z administratorem',
        })
    }
}
